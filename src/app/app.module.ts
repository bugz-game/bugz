import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { faUserTie, faUserClock, faUser } from '@fortawesome/free-solid-svg-icons';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { ActionsModule } from './actions/actions.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EventLogModule } from './event-log/event-log.module';
import { HeaderComponent } from './header/header.component';
import { ScenarioModule } from './scenario/scenario.module';
import { BugzStoreModule } from './store/bugz-store.module';
import { MatListModule } from '@angular/material/list';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    !environment.production ? StoreDevtoolsModule.instrument() : [],
    BrowserAnimationsModule,
    MatToolbarModule,
    MatListModule,
    FlexLayoutModule,
    FontAwesomeModule,
    BugzStoreModule,
    EventLogModule,
    ActionsModule,
    ScenarioModule,
  ],
  exports: [
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(library: FaIconLibrary) {
    library.addIcons(faUser, faUserClock, faUserTie);
  }
}
