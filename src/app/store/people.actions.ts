import { Action } from '@ngrx/store';

export enum PeopleActionTypes {
  HireDeveloper = '[People] Hire Developer',
  HireScrumMaster = '[People] Hire Scrum Master',
  HireProductOwner = '[People] Hire Product Owner',
  HirePeople = '[People] Hire People',
}

export class HireDeveloper implements Action {
  readonly type = PeopleActionTypes.HireDeveloper;
}
export class HireScrumMaster implements Action {
  readonly type = PeopleActionTypes.HireScrumMaster;
}
export class HireProductOwner implements Action {
  readonly type = PeopleActionTypes.HireProductOwner;
}
export class HirePeople implements Action {
  readonly type = PeopleActionTypes.HirePeople;

  public developer: number;
  public scrumMaster: number;
  public productOwner: number;
  constructor({
    developer,
    scrumMaster,
    productOwner,
  }) {
    this.developer = developer;
    this.scrumMaster = scrumMaster;
    this.productOwner = productOwner;
  }
}

export type PeopleActions
  = HireDeveloper
  | HireScrumMaster
  | HireProductOwner
  | HirePeople
  ;
