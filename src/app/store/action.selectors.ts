import { createSelector } from '@ngrx/store';
import { selectBalance } from './account.selectors';

const MIN_HIRING_BALANCE = 100000;

export const canHire = createSelector(
  selectBalance,
  (balance) => balance > MIN_HIRING_BALANCE
);
