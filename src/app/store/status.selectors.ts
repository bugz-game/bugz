import { createSelector } from '@ngrx/store';
import { State } from '.';

const selectStatus = (state: State) => state.status;

export const gameOver = createSelector(
  selectStatus,
  (state) => state.gameOver
);
