import { createSelector } from '@ngrx/store';
import { State } from '.';

const selectTime = (state: State) => state.time;

export const selectDay = createSelector(
  selectTime,
  (state) => state.day
);
