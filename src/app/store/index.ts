import {
  ActionReducerMap,
  MetaReducer
} from '@ngrx/store';

import * as fromTime from './time.reducer';
import * as fromAccount from './account.reducer';
import * as fromPeople from './people.reducer';
import * as fromScenario from './scenario.reducer';
import * as fromEventLog from './event-log.reducer';
import * as fromStatus from './status.reducer';

import { environment } from '../../environments/environment';


export interface State {
  time: fromTime.State;
  account: fromAccount.State;
  people: fromPeople.State;
  scenario: fromScenario.State;
  events: fromEventLog.State;
  status: fromStatus.State;
}

export const reducers: ActionReducerMap<State> = {
  time: fromTime.reducer,
  account: fromAccount.reducer,
  people: fromPeople.reducer,
  scenario: fromScenario.reducer,
  events: fromEventLog.reducer,
  status: fromStatus.reducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];
