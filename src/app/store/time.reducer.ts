import { Action } from '@ngrx/store';
import { AdvanceDay, TimeActionTypes } from './time.actions';


export const timeFeatureKey = 'time';

export interface State {
  day: number;
}

export const initialState: State = {
  day: 0,
};

export function reducer(state = initialState, action: Action): State {
  switch (action.type) {
    case TimeActionTypes.AdvanceDay: {
      return {
        ...state,
        day : state.day + 1,
      };
    }

    default:
      return state;
  }
}
