import { createSelector } from '@ngrx/store';
import { State } from '.';

const selectScenario = (state: State) => state.scenario;
export const selectStep = (scenario: string) =>  createSelector(
  selectScenario,
  (state) => state[scenario],
);
