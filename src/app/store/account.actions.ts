import { Action } from '@ngrx/store';


export enum AccountActionTypes {
  ReceiveSeed = '[Account] Receive Seed',
  SpendMoney = '[Account] Spend Money',
}

export class ReceiveSeed implements Action {
  public static readonly SEED_AMOUNT = 6_000_000;

  readonly type = AccountActionTypes.ReceiveSeed;
  constructor(
    public amount: number = ReceiveSeed.SEED_AMOUNT
  ) {
  }
}

export class SpendMoney implements Action {

  readonly type = AccountActionTypes.SpendMoney;
  constructor(
    public amount: number,
  ) {
  }
}


export type AccountActions
  = ReceiveSeed
  | SpendMoney
  ;
