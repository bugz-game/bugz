import { createSelector } from '@ngrx/store';
import { State } from '.';

const selectEventLog = (state: State) => state.events;

export const selectEvents = createSelector(
  selectEventLog,
  (state) => state.events
);
