import { reducer, initialState } from './scenario.reducer';
import { AdvanceScenario } from './scenario.actions';

describe('Scenario Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('AdvanceScenario action', () => {
    it('should initialize scenario', () => {
      const action = new AdvanceScenario('test');
      const result = reducer(initialState, action);
      expect(result).toEqual({ ['test']: 0 });
    });

    it('should increase step 0', () => {
      const action = new AdvanceScenario('test');
      const result = reducer({ ['test']: 0 }, action);
      expect(result).toEqual({ ['test']: 1 });
    });

    it('should increase step 1', () => {
      const action = new AdvanceScenario('test');
      const result = reducer({ ['test']: 1 }, action);
      expect(result).toEqual({ ['test']: 2 });
    });
  });

});
