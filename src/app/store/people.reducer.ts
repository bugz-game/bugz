import { Action } from '@ngrx/store';
import { PeopleActionTypes, PeopleActions } from './people.actions';


export const peopleFeatureKey = 'people';

export interface State {
  dev: number;
  po: number;
  sm: number;
}

export const initialState: State = {
  dev: 0,
  po: 0,
  sm: 0,
};

export function reducer(state = initialState, action: PeopleActions): State {
  switch (action.type) {
    case PeopleActionTypes.HireDeveloper: {
      return {
        ...state,
        dev: state.dev + 1,
      };
    }
    case PeopleActionTypes.HireScrumMaster: {
      return {
        ...state,
        sm: state.sm + 1,
      };
    }
    case PeopleActionTypes.HireProductOwner: {
      return {
        ...state,
        po: state.po + 1,
      };
    }
    case PeopleActionTypes.HirePeople: {
      return {
        ...state,
        dev: state.dev + action.developer,
        sm: state.sm + action.scrumMaster,
        po: state.po + action.productOwner,
      };
    }
    default:
      return state;
  }
}
