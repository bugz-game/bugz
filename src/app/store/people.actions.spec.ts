import * as PeopleActions from './people.actions';

describe('PeopleActions', () => {
  it('HireDeveloper should create an instance', () => {
    expect(new PeopleActions.HireDeveloper()).toBeTruthy();
  });
  it('HireScrumMaster should create an instance', () => {
    expect(new PeopleActions.HireScrumMaster()).toBeTruthy();
  });
  it('HireProductOwner should create an instance', () => {
    expect(new PeopleActions.HireProductOwner()).toBeTruthy();
  });
  it('HirePeople should create an instance', () => {
    expect(new PeopleActions.HirePeople({ developer: 1, scrumMaster: 2, productOwner: 3 })).toBeTruthy();
  });
});
