import * as EventLogActionTypes from './event-log.actions';

describe('EventLogActionTypes.AddMessage', () => {
  it('should create an instance', () => {
    expect(new EventLogActionTypes.AddMessage(1, 'test')).toBeTruthy();
    expect(new EventLogActionTypes.AddMessage(1, 'test').day).toBe(1);
    expect(new EventLogActionTypes.AddMessage(1, 'test').message).toBe('test');
  });
});
