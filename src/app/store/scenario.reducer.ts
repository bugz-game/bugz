import { ScenarioActionTypes, ScenarioActions } from './scenario.actions';


export const scenarioFeatureKey = 'scenario';

export interface State {
  [index: string]: number;
}

export const initialState: State = {

};

export function reducer(state = initialState, action: ScenarioActions): State {
  switch (action.type) {
    case ScenarioActionTypes.AdvanceScenario: {
      let step = state[action.name];
      if (step || step === 0) {
        step++;
      } else {
        step = 0;
      }
      return {
        ...state,
        [action.name]: step
      };
    }
    default:
      return state;
  }
}
