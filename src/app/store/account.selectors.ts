import { createSelector } from '@ngrx/store';
import { State } from '.';

const selectAccount = (state: State) => state.account;

export const selectBalance = createSelector(
  selectAccount,
  (state) => state.balance
);
