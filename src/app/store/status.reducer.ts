import { StatusActionTypes, StatusActions } from './status.actions';

export const statusFeatureKey = 'status';

export interface State {
  gameOver: boolean;
}

export const initialState: State = {
  gameOver: false,
};

export function reducer(state = initialState, action: StatusActions): State {
  switch (action.type) {
    case StatusActionTypes.GameOver: {
      return {
        ...state,
        gameOver: true,
      };
    }
    default:
      return state;
  }
}
