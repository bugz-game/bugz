import { Action } from '@ngrx/store';

export enum EventLogActionTypes {
  AddMessage = '[Event Log] Add Message',

}

export class AddMessage implements Action {
  readonly type = EventLogActionTypes.AddMessage;
  constructor(
    public readonly day: number,
    public readonly message: string,
  ) { }
}


export type EventLogActions
  = AddMessage
  ;
