import { Action } from '@ngrx/store';

export enum TimeActionTypes {
  AdvanceDay = '[Time] Advance Day',
}

export class AdvanceDay implements Action {
  readonly type = TimeActionTypes.AdvanceDay;
}


export type TimeActions
  = AdvanceDay
  ;
