import { Action } from '@ngrx/store';
import { Event } from '../event-log/event';
import { EventLogActionTypes, EventLogActions } from './event-log.actions';

const MAX_LOG_SIZE = 20;

export const eventLogFeatureKey = 'eventLog';

export interface State {
  events: Event[];
}

export const initialState: State = {
  events: [],
};

export function reducer(state = initialState, action: EventLogActions): State {
  switch (action.type) {
    case EventLogActionTypes.AddMessage: {
      let events = [...state.events, new Event(action.day, action.message)];
      if (events.length > MAX_LOG_SIZE) {
        events = events.slice(events.length - MAX_LOG_SIZE);
      }
      return {
        ...state,
        events,
      };
    }
    default:
      return state;
  }
}
