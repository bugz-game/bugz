import { AccountActionTypes, AccountActions } from './account.actions';

export const accountFeatureKey = 'account';

export interface State {
  balance: number;
}

export const initialState: State = {
  balance: 0,
};

export function reducer(state = initialState, action: AccountActions): State {
  switch (action.type) {
    case AccountActionTypes.ReceiveSeed: {
      return {
        ...state,
        balance: action.amount,
      };
    }
    case AccountActionTypes.SpendMoney: {
      const balance = state.balance > action.amount
        ? state.balance - action.amount
        : 0;
      return {
        ...state,
        balance
      };
    }
    default:
      return state;
  }
}
