import { reducer, initialState } from './event-log.reducer';
import { AddMessage } from './event-log.actions';
import { Event } from '../event-log/event';

describe('EventLog Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('AddMessage action', () => {
    it('should add a message', () => {
      const action = new AddMessage(1, 'test');
      const result = reducer(initialState, action);
      expect(result).toEqual({ events: [new Event(1, 'test')] });
    });

    it('should not exceed maximum log count', () => {
      let action = new AddMessage(0, 'test');
      let result = reducer(initialState, action);
      for (let i = 0; i < 25; i++) {
        action = new AddMessage(i, 'test');
        result = reducer(result, action);
      }
      expect(result).toBeTruthy();
      expect(result.events).toBeTruthy();
      expect(result.events.length).toBe(20);
      expect(result.events[0].day).toBe(5);
      expect(result.events[result.events.length - 1].day).toBe(24);
    });
  });

});
