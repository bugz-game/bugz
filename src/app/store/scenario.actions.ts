import { Action } from '@ngrx/store';

export enum ScenarioActionTypes {
  AdvanceScenario = '[Scenario] Advance Scenario',


}

export class AdvanceScenario implements Action {
  readonly type = ScenarioActionTypes.AdvanceScenario;
  constructor(
    public readonly name: string,
  ) {}
}


export type ScenarioActions
  = AdvanceScenario
  ;
