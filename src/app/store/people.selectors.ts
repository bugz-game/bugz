import { createSelector } from '@ngrx/store';
import { State } from '.';
import * as fromPeople from './people.reducer';

const DAILY_HOURS = 8;
const DEV_DAILY_COST = 100 * DAILY_HOURS;
const SM_DAILY_COST = 120 * DAILY_HOURS;
const PO_DAILY_COST = 180 * DAILY_HOURS;

const dailyCost = (state: fromPeople.State) => {
  return state.dev * DEV_DAILY_COST
    + state.sm * SM_DAILY_COST
    + state.po * PO_DAILY_COST
    ;
};

const selectPeople = (state: State) => state.people;

export const selectDevCount = createSelector(
  selectPeople,
  (state) => state.dev
);

export const selectSmCount = createSelector(
  selectPeople,
  (state) => state.sm
);

export const selectPoCount = createSelector(
  selectPeople,
  (state) => state.po
);

export const selectDailyCost = createSelector(
  selectPeople,
  (state) => dailyCost(state)
);
