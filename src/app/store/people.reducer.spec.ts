import { reducer, initialState } from './people.reducer';
import { HireDeveloper, HireProductOwner, HireScrumMaster, HirePeople } from './people.actions';

describe('People Reducer', () => {
  describe('an unknown action', () => {
    it('should return the previous state', () => {
      const action = {} as any;

      const result = reducer(initialState, action);

      expect(result).toBe(initialState);
    });
  });

  describe('HireDeveloper action', () => {
    it('should increase developer count', () => {
      const action = new HireDeveloper();
      const result = reducer(initialState, action);
      expect(result.dev).toBe(1);
    });
  });

  describe('HireScrumMaster action', () => {
    it('should increase developer count', () => {
      const action = new HireScrumMaster();
      const result = reducer(initialState, action);
      expect(result.sm).toBe(1);
    });
  });

  describe('HireProductOwner action', () => {
    it('should increase developer count', () => {
      const action = new HireProductOwner();
      const result = reducer(initialState, action);
      expect(result.po).toBe(1);
    });
  });

  describe('HirePeople action', () => {
    it('should increase individual people count', () => {
      const action = new HirePeople({ developer: 1, scrumMaster: 2, productOwner: 3 });
      const result = reducer(initialState, action);
      expect(result.dev).toBe(1);
      expect(result.sm).toBe(2);
      expect(result.po).toBe(3);
    });
  });
});
