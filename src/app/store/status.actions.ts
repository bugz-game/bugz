import { Action } from '@ngrx/store';


export enum StatusActionTypes {
  GameOver = '[Status] Game Over',

}

export class GameOver implements Action {

  readonly type = StatusActionTypes.GameOver;
}


export type StatusActions
  = GameOver
  ;
