import * as ScenarioActions from './scenario.actions';

describe('Scenario', () => {
  it('should create an instance', () => {
    expect(new ScenarioActions.AdvanceScenario('test')).toBeTruthy();
    expect(new ScenarioActions.AdvanceScenario('test').name).toBe('test');
  });
});
