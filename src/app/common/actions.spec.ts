import { doOnDay } from './actions';
import { of } from 'rxjs';

describe('doOnDay()', () => {
  it('should call the action', () => {
    let value = 0;
    const action = (day: number) => {
      value = day;
    };
    doOnDay(of(42), action);
    expect(value).toBe(42);
  });
});
