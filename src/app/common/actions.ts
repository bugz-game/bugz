import { Subject, Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

export function doOnDay(day: Observable<number>, action: (day: number) => void) {
  const dayOver = new Subject();
  day.pipe(takeUntil(dayOver))
    .subscribe(today => {
      action(today);
      dayOver.next();
      dayOver.complete();
    });
}

