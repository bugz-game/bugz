import { CheckZeroOut } from './check-zero-out';
import { TestBed } from '@angular/core/testing';
import { provideMockStore } from '@ngrx/store/testing';
import { Store } from '@ngrx/store';
import * as fromAll from '../store';

describe('CheckZeroOut', () => {
  let store: Store<fromAll.State>;
  const initialState = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
      ],
      providers: [
        provideMockStore({ initialState }),
        // other providers
      ],
    });

    store = TestBed.get(Store);
  });
  it('should create an instance', () => {
    expect(new CheckZeroOut(store)).toBeTruthy();
  });
});
