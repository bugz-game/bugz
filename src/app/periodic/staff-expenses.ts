import { Injectable } from '@angular/core';
import { PeriodicTask } from './periodic-task';
import { Store, select } from '@ngrx/store';
import { State } from '../store';
import { selectDailyCost } from '../store/people.selectors';
import { SpendMoney } from '../store/account.actions';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StaffExpenses extends PeriodicTask {

  dailyCost$ = this.store.pipe(select(selectDailyCost));

  constructor(
    private store: Store<State>,
  ) {
    super();
    this.condition = () => true;
    this.action = () => {
      const unsubscriber = new Subject();
      this.dailyCost$
        .pipe(takeUntil(unsubscriber))
        .subscribe(amount => {
          store.dispatch(new SpendMoney(amount));
          unsubscriber.next();
          unsubscriber.complete();
        });
    };
  }
}
