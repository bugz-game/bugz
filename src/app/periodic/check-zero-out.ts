import { Injectable } from '@angular/core';
import { PeriodicTask } from './periodic-task';
import { Store, select } from '@ngrx/store';
import { State } from '../store';
import { selectBalance } from '../store/account.selectors';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { GameOver } from '../store/status.actions';
import { AddMessage } from '../store/event-log.actions';
import { selectDay } from '../store/time.selectors';
import { doOnDay } from '../common/actions';

@Injectable({
  providedIn: 'root'
})
export class CheckZeroOut extends PeriodicTask {

  balance$ = this.store.pipe(select(selectBalance));
  day$ = this.store.pipe(select(selectDay));

  constructor(
    private store: Store<State>,
  ) {
    super();
    this.condition = () => true;
    this.action = () => {
      const unsubscriber = new Subject();
      this.balance$
        .pipe(takeUntil(unsubscriber))
        .subscribe(amount => {
          if (amount <= 0) {
            doOnDay(this.day$, (day) => {
              store.dispatch(new AddMessage(day, 'You ran out of money. Game Over!'));
              store.dispatch(new GameOver());
            });
          }
          unsubscriber.next();
          unsubscriber.complete();
        });
    };
  }

}
