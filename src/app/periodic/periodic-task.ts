
export class PeriodicTask {
  public condition: () => boolean;
  public action: () => void;
}
