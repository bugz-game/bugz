import { Injectable } from '@angular/core';
import { PeriodicTask } from './periodic-task';
import { StaffExpenses } from './staff-expenses';

@Injectable({
  providedIn: 'root'
})
export class PeriodicTaskService {

  private tasks: PeriodicTask[] = [];

  constructor(
    staffExpenses: StaffExpenses,
  ) {
    this.tasks.push(staffExpenses);
  }

  public add(task: PeriodicTask) {
    this.tasks.push(task);
  }

  public list() {
    return [...this.tasks];
  }

}
