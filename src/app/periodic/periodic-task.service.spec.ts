import { TestBed } from '@angular/core/testing';

import { PeriodicTaskService } from './periodic-task.service';
import { provideMockStore } from '@ngrx/store/testing';

describe('PeriodicTaskService', () => {
  const initialState = {};

  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      provideMockStore({ initialState }),
    ],
  }));

  it('should be created', () => {
    const service: PeriodicTaskService = TestBed.get(PeriodicTaskService);
    expect(service).toBeTruthy();
  });
});
