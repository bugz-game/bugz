import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { InitialOfferDialogComponent } from './initial-scenario';

@NgModule({
  declarations: [
    InitialOfferDialogComponent,
  ],
  imports: [
    CommonModule,
    MatDialogModule,
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
  ],
  entryComponents: [
    InitialOfferDialogComponent,
  ]
})
export class ScenarioModule { }
