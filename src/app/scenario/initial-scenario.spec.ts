import { InitialScenario } from './initial-scenario';
import { TestBed } from '@angular/core/testing';
import { Store } from '@ngrx/store';
import { provideMockStore, MockStore } from '@ngrx/store/testing';
import * as fromAll from '../store';
import { MatDialogModule, MatDialog } from '@angular/material/dialog';
import { CheckZeroOut } from '../periodic/check-zero-out';
import { PeriodicTaskService } from '../periodic/periodic-task.service';

describe('InitialScenario', () => {
  let store: Store<fromAll.State>;
  let dialog: MatDialog;
  let zeroOut: CheckZeroOut;
  let periodicTaskService: PeriodicTaskService;
  const initialState = {};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        MatDialogModule,
      ],
      providers: [
        provideMockStore({ initialState }),
        // other providers
      ],
    });

    store = TestBed.get(Store);
    dialog = TestBed.get(MatDialog);
    zeroOut = TestBed.get(CheckZeroOut);
    periodicTaskService = TestBed.get(PeriodicTaskService);
  });

  it('should create an instance', () => {
    expect(new InitialScenario(store, dialog, zeroOut, periodicTaskService)).toBeTruthy();
    expect(new InitialScenario(store, dialog, zeroOut, periodicTaskService).name).toBe('init');
    expect(new InitialScenario(store, dialog, zeroOut, periodicTaskService).steps).toBeTruthy();
  });
});
