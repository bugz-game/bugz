import { TestBed } from '@angular/core/testing';
import { ScenarioService } from './scenario.service';
import { provideMockStore } from '@ngrx/store/testing';
import { MatDialogModule } from '@angular/material/dialog';

describe('ScenarioService', () => {
  const initialState = {};

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      MatDialogModule,
    ],
    providers: [
      provideMockStore({ initialState }),
    ],
  }));

  it('should be created', () => {
    const service: ScenarioService = TestBed.get(ScenarioService);
    expect(service).toBeTruthy();
  });
});
