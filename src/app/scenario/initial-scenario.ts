import { select, Store } from '@ngrx/store';
import { selectDay } from '../store/time.selectors';
import { State } from '../store';
import { map } from 'rxjs/operators';
import { ReceiveSeed } from '../store/account.actions';
import { AddMessage } from '../store/event-log.actions';
import { Scenario } from './scenario';
import { Injectable, Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { GameOver } from '../store/status.actions';
import { Subject } from 'rxjs';
import { HirePeople } from '../store/people.actions';
import { doOnDay } from '../common/actions';
import { CheckZeroOut } from '../periodic/check-zero-out';
import { PeriodicTaskService } from '../periodic/periodic-task.service';

export interface DialogData {
  message: string;
  result: boolean;
}

@Component({
  selector: 'app-initial-offer-dialog',
  templateUrl: 'initial-offer-dialog.html',
})
export class InitialOfferDialogComponent {

  constructor(
    public dialogRef: MatDialogRef<InitialOfferDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData,
  ) {
    this.data.result = true;
  }

  onNoClick(): void {
    this.data.result = false;
    this.dialogRef.close();
  }

}

@Injectable({
  providedIn: 'root'
})
export class InitialScenario extends Scenario {

  day$ = this.store.pipe(select(selectDay));
  promotionCondition = new Subject<boolean>();

  constructor(
    private store: Store<State>,
    private dialog: MatDialog,
    private zeroOut: CheckZeroOut,
    private periodicTasks: PeriodicTaskService,
  ) {

    super('init');
    this.steps = [

      this.step(
        this.day$.pipe(map(day => day === 1)),
        () => {
          const message = `You have been asked to take the role of a manager in a new agile organization.`;
          store.dispatch(new AddMessage(1, message));
          const dialogRef: MatDialogRef<InitialOfferDialogComponent, boolean> = this.dialog.open(InitialOfferDialogComponent, {
            data: {
              message,
            },
          });
          dialogRef.afterClosed()
            .pipe(map((value: boolean) => {
              console.log(`result: ${value}`);
              if (value) {
                return true;
              } else {
                doOnDay(this.day$, day => {
                  this.store.dispatch(new AddMessage(day, 'You will work on your current position till you dully retire.'));
                  this.store.dispatch(new AddMessage(day, `You don't deserve to play this game. Game Over!`));
                  this.store.dispatch(new GameOver());
                });
                return false;
              }
            }))
            .subscribe(this.promotionCondition);
        }),

      this.step(
        this.promotionCondition,
        () => {
          doOnDay(this.day$, day => {
            this.store.dispatch((new HirePeople({
              developer: 50,
              productOwner: 10,
              scrumMaster: 5,
            })));
            this.store.dispatch(new AddMessage(day,
              `Congratulations! You've been promoted to be a manager in a new agile organization.`));
          });
        }
      ),

      this.step(
        this.day$.pipe(map(day => day >= 2)),
        () => doOnDay(this.day$, day => {
          this.store.dispatch(new ReceiveSeed());
          this.store.dispatch(new AddMessage(day,
            `You have received €${ReceiveSeed.SEED_AMOUNT.toLocaleString()} as seed money. Now you can hire some developers.`));
          this.periodicTasks.add(this.zeroOut);
        })),
    ];
  }

}
