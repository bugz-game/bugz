import { Observable } from 'rxjs';

export class Step {
  constructor(
    public condition: Observable<boolean>,
    public action: () => void
  ) { }
}

export class Scenario {

  public steps: Step[] = [];

  constructor(
    public name: string,
  ) { }

  public step(
    condition: Observable<boolean>,
    action: () => void,
  ) {
    return new Step(condition, action);
  }
}
