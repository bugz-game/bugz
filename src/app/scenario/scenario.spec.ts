import { Scenario } from './scenario';

describe('Scenario', () => {
  it('should create an instance', () => {
    expect(new Scenario('test')).toBeTruthy();
    expect(new Scenario('test').name).toBe('test');
  });
});
