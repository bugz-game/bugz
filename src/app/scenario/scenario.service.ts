import { Injectable } from '@angular/core';
import { Scenario } from './scenario';
import { InitialScenario } from './initial-scenario';

@Injectable({
  providedIn: 'root'
})
export class ScenarioService {
  private scenarios: Map<string, Scenario>;

  constructor(
    initial: InitialScenario,
  ) {
    this.scenarios = new Map<string, Scenario>();
    this.add(initial);
  }

  private add(scenario: Scenario) {
    this.scenarios.set(scenario.name, scenario);
  }

  public list() {
    return [...this.scenarios.values()];
  }

}
