import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { selectBalance } from '../store/account.selectors';
import { selectDay } from '../store/time.selectors';
import { State } from '../store';
import {
  selectDevCount, selectSmCount,
  selectPoCount, selectDailyCost
} from '../store/people.selectors';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  balance$ = this.store.pipe(select(selectBalance));
  day$ = this.store.pipe(select(selectDay));
  dailyCost$ = this.store.pipe(select(selectDailyCost));
  devCount$ = this.store.pipe(select(selectDevCount));
  smCount$ = this.store.pipe(select(selectSmCount));
  poCount$ = this.store.pipe(select(selectPoCount));

  constructor(
    private store: Store<State>
  ) { }

  ngOnInit() {
  }

}
