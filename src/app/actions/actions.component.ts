import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State } from '../store';
import { canHire } from '../store/action.selectors';
import { HireDeveloper, HireScrumMaster, HireProductOwner } from '../store/people.actions';

@Component({
  selector: 'app-actions',
  templateUrl: './actions.component.html',
  styleUrls: ['./actions.component.scss']
})
export class ActionsComponent implements OnInit {

  canHire$ = this.store.pipe(select(canHire));

  constructor(
    private store: Store<State>
  ) { }

  ngOnInit() {
  }

  doHireDeveloper() {
    this.store.dispatch(new HireDeveloper());
  }

  doHireScrumMaster() {
    this.store.dispatch(new HireScrumMaster());
  }

  doHireProductOwner() {
    this.store.dispatch(new HireProductOwner());
  }
}
