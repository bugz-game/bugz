import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ActionsComponent } from './actions.component';
import { MatButtonModule } from '@angular/material/button';
import { FontAwesomeModule, FaIconLibrary } from '@fortawesome/angular-fontawesome';
import { MatListModule } from '@angular/material/list';



@NgModule({
  declarations: [
    ActionsComponent,
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatListModule,
    FontAwesomeModule,
  ],
  exports: [
    ActionsComponent,
  ]
})
export class ActionsModule { }
