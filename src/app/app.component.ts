import { Component, OnInit } from '@angular/core';
import { GameEngineService } from './engine/game-engine.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'bugz';

  constructor(
    private engine: GameEngineService,
  ) {
  }

  ngOnInit(): void {
    this.engine.start();
  }
}
