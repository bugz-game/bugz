import { async, TestBed } from '@angular/core/testing';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { RouterTestingModule } from '@angular/router/testing';
import { ActionsComponent } from './actions/actions.component';
import { AppComponent } from './app.component';
import { EventLogComponent } from './event-log/event-log.component';
import { HeaderComponent } from './header/header.component';
import { BugzStoreModule } from './store/bugz-store.module';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReversePipe } from './event-log/reverse.pipe';

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        MatToolbarModule,
        MatListModule,
        MatIconModule,
        FontAwesomeModule,
        MatDialogModule,
        BugzStoreModule,
      ],
      declarations: [
        AppComponent,
        ActionsComponent,
        HeaderComponent,
        EventLogComponent,
        ReversePipe,
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'bugz'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('bugz');
  });

});
