import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { EventLogComponent } from './event-log.component';
import { BugzStoreModule } from '../store/bugz-store.module';
import { ReversePipe } from './reverse.pipe';


describe('EventLogComponent', () => {
  let component: EventLogComponent;
  let fixture: ComponentFixture<EventLogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        EventLogComponent,
        ReversePipe,
      ],
      imports: [
        MatCardModule,
        MatListModule,
        MatIconModule,
        BugzStoreModule,
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventLogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
