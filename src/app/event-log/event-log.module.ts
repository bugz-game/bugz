import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { EventLogComponent } from './event-log.component';
import { StoreModule } from '@ngrx/store';
import { ReversePipe } from './reverse.pipe';



@NgModule({
  declarations: [
    EventLogComponent,
    ReversePipe,
  ],
  imports: [
    CommonModule,
    MatCardModule,
    MatListModule,
    MatIconModule,
    StoreModule,
  ],
  exports: [
    EventLogComponent,
  ],
})
export class EventLogModule { }
