import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { State } from '../store';
import { selectEvents } from '../store/event-log.selectors';
import { ReversePipe } from './reverse.pipe';

@Component({
  selector: 'app-event-log',
  templateUrl: './event-log.component.html',
  styleUrls: ['./event-log.component.scss'],
})
export class EventLogComponent implements OnInit {

  events$ = this.store.pipe(select(selectEvents));

  constructor(
    private store: Store<State>
  ) { }

  ngOnInit() {
  }

}
