import { Event } from './event';

describe('Event', () => {
  it('should create an instance', () => {
    expect(new Event(2, 'test')).toBeTruthy();
    expect(new Event(42, 'test').day).toBe(42);
    expect(new Event(42, 'test').message).toBe('test');
  });
});
