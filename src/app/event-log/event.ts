export class Event {
  constructor(
    public day: number,
    public message: string,
  ) { }
}
