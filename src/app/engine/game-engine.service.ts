import { Injectable } from '@angular/core';
import { timer, Observable, Subscription, Subject } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { State } from '../store';
import { AdvanceDay } from '../store/time.actions';
import { ScenarioService } from '../scenario/scenario.service';
import { selectStep } from '../store/scenario.selectors';
import { AdvanceScenario } from '../store/scenario.actions';
import { Scenario, Step } from '../scenario/scenario';
import { takeUntil } from 'rxjs/operators';
import { gameOver } from '../store/status.selectors';
import { PeriodicTaskService } from '../periodic/periodic-task.service';

const INTERVAL = 2000;

@Injectable({
  providedIn: 'root'
})
export class GameEngineService {
  private ticker: Observable<number>;
  private ticking: Subscription;
  public scenarios: Subscription[];
  private gameOver$ = this.store.pipe(select(gameOver));

  constructor(
    private store: Store<State>,
    private scenarioService: ScenarioService,
    private periodicTaskService: PeriodicTaskService,
  ) {
    this.ticker = timer(0, INTERVAL);
    this.scenarios = [];
  }

  private initializeScenario(scenario: Scenario) {
    this.store.dispatch(new AdvanceScenario(scenario.name));
  }

  start() {
    if (!this.ticking) {
      this.gameOver$.subscribe(gameIsOver => {
        if (gameIsOver) { this.stop(); }
      });

      this.scenarioService.list().forEach((scenario) => {
        this.initializeScenario(scenario);
        this.scenarios[scenario.name] = this.store
          .pipe(select(selectStep(scenario.name)))
          .subscribe(stepIndex => {
            if (this.checkIfScenarioFinished(scenario, stepIndex)) {
              this.unsubscibeScenario(scenario, stepIndex);
            } else {
              this.checkAndPerformStep(scenario, stepIndex);
            }
          });
      });

      this.ticking = this.ticker.subscribe((time) => {
        this.periodicTaskService.list()
          .forEach(task => {
            if (task.condition()) {
              task.action();
            }
          });
        this.store.dispatch(new AdvanceDay());
      });
    }
  }

  private unsubscibeScenario(scenario: Scenario, stepIndex: number) {
    console.log(`@${scenario.name}[${stepIndex}] scenario finished.`);
    const subscription = this.scenarios[scenario.name];
    if (subscription) {
      subscription.unsubscribe();
      delete this.scenarios[scenario.name];
    }
  }

  private checkIfScenarioFinished(scenario: Scenario, stepIndex: number) {
    if (stepIndex >= scenario.steps.length) {
      return true;
    }
    return false;
  }

  private checkAndPerformStep(scenario: Scenario, stepIndex: number) {
    console.log(`@${scenario.name}[${stepIndex}] checking...`);
    const step = scenario.steps[stepIndex];
    if (step) {
      const unsubscriber = new Subject();
      step.condition
        .pipe(takeUntil(unsubscriber))
        .subscribe(fulfilled => {
          console.log(`@${scenario.name}[${stepIndex}] condition fulfilled: ${fulfilled}`);
          if (fulfilled) {
            console.log(`@${scenario.name}[${stepIndex}] performing action ...`);
            step.action();
            this.store.dispatch(new AdvanceScenario(scenario.name));
            unsubscriber.next();
            unsubscriber.complete();
          }
        });
    }
  }

  public started(): boolean {
    return this.ticking !== undefined;
  }

  public stop() {
    if (this.ticking) {
      this.ticking.unsubscribe();
      this.ticking = undefined;
    }
  }
}
