import { TestBed } from '@angular/core/testing';

import { GameEngineService } from './game-engine.service';
import { BugzStoreModule } from '../store/bugz-store.module';
import { ScenarioService } from '../scenario/scenario.service';
import { Scenario } from '../scenario/scenario';
import { Observable } from 'rxjs';

class TestScenario extends Scenario {
  step1 = false;
  step2 = false;
  step1Condition = TestScenario.advance(1);
  step2Condition = TestScenario.advance(2);

  static advance = (treshold: number) => {
    return new Observable<boolean>((observer) => {
      let value = 0;
      const interval = setInterval(() => {
        observer.next(false);
        if (value > treshold) {
          observer.next(true);
        }
        value++;
      }, 10);

      return () => clearInterval(interval);
    });
    // ghuio
  }

  constructor() {
    super('test');
    this.steps = [
      this.step(this.step1Condition, () => this.step1 = true),
      this.step(this.step2Condition, () => this.step2 = true),
    ];
  }
}

describe('GameEngineService', () => {
  let testScenario: TestScenario;

  beforeEach(() => {
    testScenario = new TestScenario();
    const scenarioService = jasmine.createSpyObj('ScenarioService', ['list']);
    scenarioService.list.and.returnValue([
      testScenario,
    ]);

    TestBed.configureTestingModule({
      imports: [
        BugzStoreModule,
      ],
      providers: [
        { provide: ScenarioService, useValue: scenarioService }
      ]
    });
  });

  it('should be created', () => {
    const service: GameEngineService = TestBed.get(GameEngineService);
    expect(service).toBeTruthy();
  });

  it('should not be started initially', () => {
    const service: GameEngineService = TestBed.get(GameEngineService);
    expect(service.started()).toBeFalsy();
  });

  it('can be started', () => {
    const service: GameEngineService = TestBed.get(GameEngineService);
    service.start();
    expect(service.started()).toBeTruthy();
    service.stop();
  });

  it('can be started and stopped', () => {
    const service: GameEngineService = TestBed.get(GameEngineService);
    service.start();
    service.stop();
    expect(service.started()).toBeFalsy();
  });

  it('finishes scenario', () => {
    const service: GameEngineService = TestBed.get(GameEngineService);
    service.start();
    expect(service.scenarios).toBeDefined();
    expect(service.scenarios.length).toEqual(0);
    service.stop();
  });
});
